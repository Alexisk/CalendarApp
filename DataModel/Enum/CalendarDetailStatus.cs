﻿namespace DataModel.Enum
{
  public enum CalendarDetailStatus
  {
    Pending,
    Approved,
    Cancelled,
    Concluded
  }
}
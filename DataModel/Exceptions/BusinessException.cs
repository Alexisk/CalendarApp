﻿using System;

namespace DataModel.Exceptions
{
	public class BusinessException : Exception
	{
		private readonly string InnerMessage;

		public BusinessException(string message)
		{
			this.InnerMessage = message;
		}

		public override string Message => $"Error en la capa de negocio. {this.InnerMessage}";
	}
}
﻿using System;

namespace DataModel
{
	public class DaoException : Exception
	{
		private readonly string InnerMessage;

		public DaoException(string message)
		{
			this.InnerMessage = message;
		}

		public override string Message => $"Error en la conexion con la base. {this.InnerMessage}";
	}
}
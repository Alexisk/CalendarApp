﻿using System;
using System.Collections.Generic;
using DataModel.Entities;

namespace DataModel.DTO
{
  public class CalendarInfo
  {
    public DayOfWeek DayWeekNumber { get; set; }
    public int DayNumber { get; set; }
    public Calendar Calendar { get; set; }
    public List<CalendarDetail> Details { get; set; }
		public string HolidayDescription { get; set; }
		public bool IsHoliday { get; set; }
	}
}
﻿using DataModel.Enum;

namespace DataModel.Entities
{
  public class Contact
  {
    public ContactType Id { get; set; }

    public string Name { get; set; }
  }
}
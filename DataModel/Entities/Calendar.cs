﻿using DataModel.Entities;
using System;

namespace DataModel.Entities
{
  public class Calendar
  {
    public long Id { get; set; }

    public DateTime Date { get; set; }

    public int Year { get; set; }

    public int Month { get; set; }

    public int Day { get; set; }

    public int ElapsedTime { get; set; }
  }
}

﻿using System.Collections.Generic;

namespace DataModel.Entities
{
  public class Person
  {
    public long Id { get; set; }

    public string Name { get; set; }

    public string LastName { get; set; }

    public IList<ContactPerson> Contacts { get; set; }
  }
}
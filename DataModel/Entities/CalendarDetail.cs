﻿using System;
using DataModel.Enum;

namespace DataModel.Entities
{
  public class CalendarDetail
  {
	  public long? Id { get; set; }

		public Calendar Calendar { get; set; }

    public string Description { get; set; }
    
    public int? Reminder { get; set; }

    public CalendarDetailStatus? Status { get; set; }
  }
}

﻿using DataModel.Enum;

namespace DataModel.Entities
{
  public class ContactPerson
  {
    public long Id { get; set; }

    public Person Person { get; set; }

    public Contact Contact { get; set; }

    public string Value { get; set; }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataModel.Entities
{
	public class Holiday
	{
		public string Name { get; set; }

		public DateTime	Date { get; set; }
	}
}

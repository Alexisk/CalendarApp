﻿using System;
using DataModel.Enum;

namespace DataModel.Entities
{
  public class CalendarPerson
  {
    public long Id { get; set; }

    public Calendar Calendar { get; set; }

    public Person Person { get; set; }

  }
}

# Calendar APP

## Introducción

Esta aplicación es una agenda electrónica, permite dar de alta modificar y borrar tanto personas como reuniones.
Desde la pantalla principal se puede visualizar los diferentes meses y poder ver las reuniones por cada día y su estado.
Por otro lado, se puede dar de alta los feriados a través de un archivo.

## Para utilizar la aplicación

Para correr la aplicación se debe previamente instanciar la base de dato según el proyecto de base de datos. 
Posteriormente correr el script ConfigurationDataScript.sql para los datos de configuración.

Nota: para ver datos de ejemplo se puede cargar el script: ExampleDataScript.sql

Por último se debe modificar el archivo appsettings.json para setear el connectionstring

## Arquitectura

La aplicación fue desarrollada en ASP.NET CORE 2.2 bajo el patrón MVC.
Además se utiliza Serilog para notificar por archivo y por consola lo que sucede con la aplicación (en remplazo de Log4Net).

El proyecto se divide en 3 capas:

- Dao: componente dedicado a la conexión con la base de datos.
- BusinessLogic: Capa dedicada a la lógica referente a la funcionalidad de la APP.
- CalendarApp: User interface de la aplicación
- DataModel: Componente común con las definiciones de las entidades.
- CalendarDB: componente de base de datos con las definiciones de la base.

### Dao

La capa DAO posee una implementación generíca la cual permite un cierto nivel de abstracción entre la base de datos y las entidades.
la clase Dao.cs posee acceso a la base para obtener, actualizar y eliminar datos de forma generica utilizando Reflexion. El mismo permite mantener un cierto nivel de relación siempre que sea del tipo 1-1 de forma automatica.

para poder tener este nivel de abstracción se implementó ciertas convenciones:

- Las tablas y las entidades poseen nombres idénticos entre sí.
- Las entidades tienen un identificador llamado Id
- Las relaciones 1-1 entre tablas se realizan a través de una columna "Id" con una columna "TablaId", de esta forma se pueden declarar entidades dentro de entidades y poder hacer la llamada al componente Dao que corresponda y obtener los datos de la clase principal y de las clases propiedades.

### BusinessLogic

Esta capa posee 2 clases que manejan la gestión de personas y la gestión del calendario.
Las clases se acceden a través de sus respectivas interfaces. Instanciadas gracias al contenedor propio de NetCore.
Ambas clases poseen excepciones personalizadas para identificar errores en la capa que corresponde. 
Por otro, se manejan transacciones para establecer consistencia entre los datos de la base.

### CalendarApp

La capa de interfaz de usuario implementa el patrón MVC provisto por Asp Net Core. 
El layout define una partial view con un pequeño menu con la siguiente información:

- Home: muestra el calendario.
- Registrar Persona: donde se pueden dar de alta personas.
- Listas Personas: Se puede listar las personas, editar y dar de baja
- Agendar Reunión: Permite anotar una nueva reunión
- Listar Reuniones: Lista las reuniones, permite modificar o borrar.
- Importar Feriados: Se importa feriados a través de un xml, se puede descargar un template para cargar los nuevos.

Hay un manejo de error tanto para los envíos de Forms que notifica en la pantalla como para los errores generales que redirigen a una view de error que muestra la información en detalle.

### DataModel

Acá se persisten las entidades las cuales son un reflejo de la base de datos.
Por otro lado, se declaró un helper con datos adiciones para el uso del componente Dao.

### CalendarDB

Proyecto de base de datos para alinear la base de datos con las definiciones del proyecto. 
Además, está incluido dos scripts con datos para la carga inicial y la prueba.

## Funcionalidad

A continuación, se detalla una descripción de la funcionalidad esperada en cada pantalla.

### Calendario (Home)

Desde esta pantalla se visualiza el mes con las reuniones agendadas.
Los días marcados en violeta muestran que hay reuniones para ese día.
Los días con un borde azul simbolizan los feriados.
Al seleccionar un día se desplaza un menú con el detalle del día. 
Si hay una reunión se pinta la franja de color que corresponda con el tiempo determinado. Se muestra además la descripción y los participantes de la reunión, eso puede exceder el espacio pintado de la reunión.
La reunión va a estar en color según el estado de la misma.

### Registrar Persona

Desde esta pantalla se permite registrar o editar una persona.
No se puede registrar 2 personas con el mismo nombre
Se debe cargar al menos un contacto, sea teléfono, celular o mail.
Cada campo debe tener el formato correspondiente.
Al finalizar se mostrará un mensaje de éxito.

### Listar Personas

Se visualiza todas las personas registradas, se puede editar (redirige a la pantalla anterior) o borrar una persona existente.
si la persona tiene una reunión asignada no puede borrarse, se mostrará un error en ese caso.

### Agendar reunión

Permite agendar una nueva reunión.
Se debe escribir una descripción, fecha, hora, tiempo y participantes.
No se puede crear una reunión para un solo participante
La reunión queda en estado pendiente.
El tiempo de la reunión debe ser mayor a media hora. (la visualización en la pantalla de Home pinta franjas con intervalos de media hora)
Se puede crear 2 reuniones para el mismo momento.
Se pueden crear reuniones en cualquier día y horario.

### Listar reuniones

Visualiza las reuniones creadas y permite editarlas, cambiarle es estado entre otras cosas.

### Importar Feriados

Desde esta pantalla se pueden importar los feriados.
Previamente se debe descargar el template como modelo para poder cargar los feriados.
Cada vez que se carga un feriado se borran todos los feriados anteriores.
Se debe seleccionar el archivo editado y apretar el botón cargar. Si todo salió bien se pueden ver los nuevos feriados en el home.






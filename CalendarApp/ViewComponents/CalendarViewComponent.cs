﻿using CalendarApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalendarApp.ViewComponents
{
  public class CalendarInfoViewComponent : ViewComponent
  {
    public async Task<IViewComponentResult> InvokeAsync()
    {
      var model = new CalendarModel();
      model.DateTime = DateTime.Now;
      return View("Component/CalendarInfo.cshtml", model);
    }
  }
}

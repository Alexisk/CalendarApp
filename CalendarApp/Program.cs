﻿using BusinessLogic;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace CalendarApp
{
  public class Program
  {
    public static void Main(string[] args)
    {
			CreateWebHostBuilder(args).Build().Run();
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>()
            .SuppressStatusMessages(true)
            .UseSerilog()
						.ConfigureServices((ctx, c) => {
              c.AddSingleton<IPersonManagement, PersonManagement>();
              c.AddSingleton<ICalendarManagement, CalendarManagement>();
						});
  }
}

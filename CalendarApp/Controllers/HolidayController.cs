﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using BusinessLogic;
using CalendarApp.Helper;
using Microsoft.AspNetCore.Mvc;
using CalendarApp.Models;
using Dao;
using DataModel.Entities;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CalendarApp.Controllers
{
  public class HolidayController : Controller
  {
	  public ICalendarManagement CalendarManagement { get; set; }

	  public HolidayController(ICalendarManagement calendarManagement)
	  {
		  CalendarManagement = calendarManagement;

	  }

		public IActionResult Index()
	  {
		  return View();
	  }

	  public FileResult DownloadTemplate()
	  {
		  try
		  {
			  var foo = new HolidayData();
			  var fooList = new List<Holiday>();
			  fooList.Add(new Holiday() { Name = "Descripción del feriado", Date = DateTime.MaxValue.Date });
			  fooList.Add(new Holiday() { Name = "Descripción del feriado", Date = DateTime.MaxValue.Date });
			  foo.Data = fooList.ToArray();


			  var serializer = new XMLSerializer();
			  var result = serializer.Serialize(foo);


			  MemoryStream stream = new MemoryStream();
			  StreamWriter writer = new StreamWriter(stream);
			  writer.Write(result);
			  writer.Flush();
			  stream.Position = 0;

				return File(stream, "text/xml", "Template.xml");

			}
			catch (Exception e)
		  {
			  Console.WriteLine(e);
			  throw;
		  }
		  
	  }

		[HttpPost("ImportHoliday")]
		public IActionResult Upload(IFormFile file)
		{
			var form = new FormResult();
			try
			{
				if (file == null || file.Length == 0)
					throw new Exception("Debe serleccionar un archivo no vacio.");
				
				var result = new HolidayData();

				using (var reader = file.OpenReadStream())
				using (var strReader = new StreamReader(reader))
				{
					var str = strReader.ReadToEndAsync().Result;
					var serializer = new XMLSerializer();
					result = serializer.Deserialize<HolidayData>(str);

				}

				CalendarManagement.ImportHolidays(result.Data);

				form.Message = $"Archivo importado correctamente";
				form.Success = true;

				return View("Index", form);
			}
	    catch(Exception ex)
	    {
		    form.Message = $"Error al importar el archivo. {ex.Message}";
		    form.Success = false;

		    return View("Index", form);
	    }

		}
    
  }
}

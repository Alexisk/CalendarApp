﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic;
using CalendarApp.Models;
using Dao;
using Microsoft.AspNetCore.Mvc;

namespace CalendarApp.Controllers
{
  public class PersonController : Controller
  {
    public IPersonManagement Management { get; set; }

    public PersonController(IPersonManagement management)
    {
      Management = management;
    }
		
    public IActionResult Edit(string id)
    {
      return View();
    }

		public IActionResult List()
    {
      var persons = Management.GetAllPersons();
      return View("List",persons.ToList());
    }


		public IActionResult Add(long? id)
		{
			var model = new PersonModel();
			
			if (id.HasValue)
			{
				var person = Management.GetPersonById(id.Value);
				model.Name = person.Name;
				model.LastName = person.LastName;
				model.Cellphone = person.Contacts.FirstOrDefault(x => x.Contact.Name == "Celular")?.Value;
				model.Mail = person.Contacts.FirstOrDefault(x => x.Contact.Name == "Email")?.Value;
				model.Phone = person.Contacts.FirstOrDefault(x => x.Contact.Name == "Telefono")?.Value;
				model.PersonId = person.Id;
			}
			
			return View(model);
		}


		[HttpPost]
		public IActionResult Register(PersonModel model)
		{
			FormResult result = new FormResult();
			try
			{
				if (!ModelState.IsValid)
				{
					model.Result = new FormResult() { Success = false, Message = "Comprobar los campos invalidos" };
						return View("Add", model);
				}

				if (model.PersonId.HasValue)
				{
					Management.UpdateRegister(model.PersonId.Value, model.Name, model.LastName, model.Phone, model.Cellphone, model.Mail);
					result = new FormResult() { Success = true, Message = "Se actualizó la persona exitosamente." };
					return List();
				}
				else
				{
					Management.SaveRegister(model.Name, model.LastName, model.Phone, model.Cellphone, model.Mail);
					result = new FormResult() { Success = true, Message = "Se cargo la persona exitosamente." };
				}

				model = new PersonModel();
			}
			catch (Exception e)
			{
				result = new FormResult() { Success = false, Message = e.Message };
			}

			model.Result = result;
			return View("Add", model);

		}

		public IActionResult Delete(long id)
		{
			Management.DeleteRegister(id);
			return List();
		}
		
	}
}
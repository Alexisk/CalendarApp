﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CalendarApp.Models;
using Dao;
using Microsoft.AspNetCore.Diagnostics;

namespace CalendarApp.Controllers
{
  public class HomeController : Controller
  {
    public IActionResult Index()
    {
      return RedirectToAction("Index", "Calendar");
    }
    
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
	    var pathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
	    Exception exception = pathFeature?.Error; // Here will be the exception details.

			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier, Message = exception.Message});
    }
  }
}

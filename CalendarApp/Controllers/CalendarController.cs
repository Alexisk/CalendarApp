﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic;
using CalendarApp.Models;
using Dao;
using DataModel.Entities;
using DataModel.Enum;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace CalendarApp.Controllers
{
	public class CalendarController : Controller
	{
		public ICalendarManagement CalendarManagement { get; set; }

		public IPersonManagement PersonManagement { get; set; }

		public CalendarController(ICalendarManagement calendarManagement, IPersonManagement personManagement)
		{
			CalendarManagement = calendarManagement;
			PersonManagement = personManagement;
		}
		
		public IActionResult Index(int? year, int? month)
		{
			if (!year.HasValue)
				year = DateTime.Now.Year;

			if (!month.HasValue)
				month = DateTime.Now.Month;


			var model = new CalendarModel();
			model.CalendarInfo = CalendarManagement.GetMonthInfo(year.Value, month.Value);
			model.DateTime = new DateTime(year.GetValueOrDefault(), month.GetValueOrDefault(), 1);

			return View("Index", model);
		}

		public IActionResult Detail(int year, int month, int day)
		{
			var detailModel = new DetailModel();

			var detailMeetings = CalendarManagement.GetDetails(year, month, day);

			var list = new List<DetailDay>();
			var dtFrom = new DateTime(year, month, day);
			var step = 48;

			for (int i = 0; i < step; i++)
			{
				var mStep = (double)(24 * 60) / (double)step;
				var minuteStep = (double)i * mStep;
				var dateStepFrom = dtFrom.AddMinutes(minuteStep);
				var dateStepTo = dtFrom.AddMinutes(minuteStep + mStep);


				var detailItem = new DetailDay();
				detailItem.Hour = dateStepFrom.Hour;
				detailItem.Minutes = dateStepFrom.Minute;


				foreach (var meeting in detailMeetings)
				{
					var meetingFrom = meeting.Calendar.Date;
					var meetingTo = meeting.Calendar.Date.AddMinutes(meeting.Calendar.ElapsedTime);

					if (meetingFrom <= dateStepFrom && meetingTo >= dateStepTo)
					{
						var item = list.FirstOrDefault(x => x.Id == meeting.Calendar.Id);

						if (item == null)
						{
							detailItem.Id = meeting.Calendar.Id;
							detailItem.Description = meeting.Description;
							detailItem.ElapsedTime = meeting.Calendar.ElapsedTime;
							detailItem.Entries = CalendarManagement.GetEntriesForId(meeting.Calendar.Id);
						}
						detailItem.Status = meeting.Status;

					}
				}

				list.Add(detailItem);
			}

			detailModel.List = list;
			
			return PartialView("Detail", detailModel);
		}
		
		public IActionResult List()
		{
			var list = CalendarManagement.GetDetails();

			return View("List", list.ToList());
		}

		public IActionResult MainPage(string value)
		{
			if (string.IsNullOrEmpty(value))
				return Index(null, null);
			else
				return PartialView(value);
		}
		
		public IActionResult Appointment(long? id)
		{
			var model = new AppointmentModel();
			SetModelList(model);

			if (id.HasValue)
			{
				var detail = CalendarManagement.GetCalendarWithDetailById(id.Value);
				model.CalendarDetailId = detail.Id;
				model.Description = detail.Description;
				model.Date = detail.Calendar.Date;
				model.ElapsedTime = detail.Calendar.ElapsedTime;
				model.Time = detail.Calendar.Date.TimeOfDay;

				var entries = CalendarManagement.GetEntriesForId(detail.Calendar.Id);
				model.Entry = entries.ToList().Select(x => x.Id).ToArray();
				model.Status = detail.Status.GetValueOrDefault(CalendarDetailStatus.Pending);
				//set current ID & current status
			}

			
			return View(model);
		}

		[HttpPost("FromBody")]
		public IActionResult NewAppointment(AppointmentModel model)
		{
			FormResult result = new FormResult();
			try
			{
				// Message = Request.Form[nameof(Message)];
				if (!ModelState.IsValid)
				{
					SetModelList(model);
					model.Result = new FormResult() { Success = false, Message = "Comprobar los campos invalidos" };
					return View("Appointment", model);
				}
				
				var date = model.Date;
				date = date.Add(model.Time);
				
				if (model.CalendarDetailId.HasValue)
				{
					CalendarManagement.UpdateAppointment(model.CalendarDetailId.Value, model.Status, model.Description, date, model.ElapsedTime, model.Entry.ToList());
					//result = new FormResult() { Success = true, Message = "Se actualizó la reunion exitosamente." };
					return List();
				}
				else
				{
					CalendarManagement.SaveAppointment(model.Description, date, model.ElapsedTime, model.Entry.ToList());
					result = new FormResult() { Success = true, Message = "Se cargo la reunion exitosamente." };
				}

				model = new AppointmentModel();
			}
			catch (Exception e)
			{
				result = new FormResult(){Success = false, Message =  e.Message};
			}
			
			model.Result = result;
			SetModelList(model);
			return View("Appointment", model);

		}
		
		public IActionResult Delete(long detailId)
		{
			CalendarManagement.DeleteAppointment(detailId);
			return List();
		}


		private void SetModelList(AppointmentModel model)
		{
			model.Entries = PersonManagement.GetAllPersons().Select(x => new SelectListItem($"{x.LastName} - {x.Name}", x.Id.ToString())).ToList();
			model.Date = DateTime.Now;
			model.TimeTable = new List<SelectListItem>();

			for (int i = 0; i < 24; i++)
			{
				for (int a = 0; a <= 30; a += 30)
				{
					model.TimeTable.Add(new SelectListItem($"{i:D2}:{a:D2}", new TimeSpan(i, a, 0).ToString()));
				}
			}

		}
	}
}
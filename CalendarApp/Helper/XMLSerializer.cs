﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CalendarApp.Helper
{
	
	public class XMLSerializer
	{
		public T Deserialize<T>(string input) where T : class
		{
			XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

			using (StringReader sr = new StringReader(input))
			{
				return (T)ser.Deserialize(sr);
			}
		}

		public string Serialize<T>(T ObjectToSerialize)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

			using (StringWriter textWriter = new StringWriter())
			{
				xmlSerializer.Serialize(textWriter, ObjectToSerialize);
				return textWriter.ToString();
			}
		}
	}
}

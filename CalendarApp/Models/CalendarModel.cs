﻿using System;
using System.Collections.Generic;
using DataModel.DTO;

namespace CalendarApp.Models
{
  public class CalendarModel
  {
    public DateTime DateTime { get; set; }

    public IList<CalendarInfo> CalendarInfo { get; set; }
  }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CalendarApp.Controllers;
using DataModel.Enum;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CalendarApp.Models
{
	public class PersonModel
	{
		public long? PersonId { get; set; }
		
		[Required]
		[Display(Name = "Nombre:")]
		public string Name { get; set; }

		[Required]
		[Display(Name = "Apellido:")]
		public string LastName { get; set; }

		[Display(Name = "Teléfono:")]
		[RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{4})$", ErrorMessage = "No es un número de teléfono valido 4444-4444")]
		public string Phone { get; set; }

		[Display(Name = "Celular:")]
		[RegularExpression(@"^\(?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$", ErrorMessage = "No es un número de celuar valido 11-4444-4444")]
		public string Cellphone { get; set; }
		
		[Display(Name = "Email:")]
		[EmailAddress(ErrorMessage = "Ingrese un email valido")]
		public string Mail { get; set; }
		
		public FormResult Result { get; set; }
		
	}
}
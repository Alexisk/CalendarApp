﻿using System.Collections.Generic;
using CalendarApp.Controllers;
using DataModel.Entities;

namespace CalendarApp.Models
{
  public class HolidayData
	{
    public Holiday[] Data { get; set; }
  }
}
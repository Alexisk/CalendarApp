﻿using System.Collections.Generic;
using DataModel.Entities;
using DataModel.Enum;

namespace CalendarApp.Models
{
  public class DetailDay
  {
    public int Hour { get; set; }
    
    public int Minutes { get; set; }

    public long? Id { get; set; }

    public CalendarDetailStatus? Status { get; set; }

    public string Description { get; set; }

    public IList<Person> Entries { get; set; }

    public int ElapsedTime { get; set; }
  }
}
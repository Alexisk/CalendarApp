﻿namespace CalendarApp.Models
{
	public class FormResult
	{
		public string Message { get; set; }

		public bool Success { get; set; }
	}
}
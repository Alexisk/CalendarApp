﻿using System.Collections.Generic;
using CalendarApp.Controllers;

namespace CalendarApp.Models
{
  public class DetailModel 
  {
    public List<DetailDay> List { get; set; }
  }
}
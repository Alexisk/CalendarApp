﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CalendarApp.Controllers;
using DataModel.Enum;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CalendarApp.Models
{
	public class AppointmentModel
	{
		
		[Display(Name = "Seleccione los integrantes")]
		public List<SelectListItem> Entries { get; set; }

		public List<SelectListItem> TimeTable { get; set; }

		[Required]
		[MinLength(2, ErrorMessage = "No puede haceer una reunion de una sola persona")]
		[Display(Name = "Participantes:")]
		public long[] Entry { get; set; }

		[Required]
		[Display(Name = "Descripción:")]
		public string Description { get; set; }

		[Required]
		[Range(30, int.MaxValue, ErrorMessage = "La reunion debe ser mayor o igual a media hora")]
		[Display(Name = "Tiempo estimado(min): ")]
		public int ElapsedTime { get; set; }

		[Required]
		[Display(Name = "Hora:")]
		public TimeSpan Time { get; set; }

		[Required]
		[Display(Name = "Fecha:")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime Date { get; set; }

		public FormResult Result { get; set; }

		public long? CalendarDetailId { get; set; }

		[Display(Name = "Estado:")]
		public CalendarDetailStatus Status { get; set; }
	}
}
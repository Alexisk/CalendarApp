USE [CalendarDB]
GO
SET IDENTITY_INSERT [dbo].[Person] ON 

INSERT [dbo].[Person] ([Id], [Name], [LastName]) VALUES (1, N'Juan', N'Perez')
INSERT [dbo].[Person] ([Id], [Name], [LastName]) VALUES (2, N'Carolina', N'Schneider')
INSERT [dbo].[Person] ([Id], [Name], [LastName]) VALUES (3, N'Carlos', N'Díaz')
INSERT [dbo].[Person] ([Id], [Name], [LastName]) VALUES (9, N'Jose', N'Gomez')
INSERT [dbo].[Person] ([Id], [Name], [LastName]) VALUES (10, N'Lorena (ext)', N'Toledo')
SET IDENTITY_INSERT [dbo].[Person] OFF
SET IDENTITY_INSERT [dbo].[Calendar] ON 

INSERT [dbo].[Calendar] ([Id], [Date], [Day], [Month], [Year], [ElapsedTime]) VALUES (4, CAST(N'2019-08-30T15:00:00.000' AS DateTime), 30, 8, 2019, 30)
INSERT [dbo].[Calendar] ([Id], [Date], [Day], [Month], [Year], [ElapsedTime]) VALUES (7, CAST(N'2019-10-10T10:00:00.000' AS DateTime), 10, 10, 2019, 180)
INSERT [dbo].[Calendar] ([Id], [Date], [Day], [Month], [Year], [ElapsedTime]) VALUES (28, CAST(N'2019-11-13T09:00:00.000' AS DateTime), 13, 11, 2019, 60)
INSERT [dbo].[Calendar] ([Id], [Date], [Day], [Month], [Year], [ElapsedTime]) VALUES (29, CAST(N'2019-11-15T13:30:00.000' AS DateTime), 15, 11, 2019, 180)
INSERT [dbo].[Calendar] ([Id], [Date], [Day], [Month], [Year], [ElapsedTime]) VALUES (34, CAST(N'2019-11-19T04:00:00.000' AS DateTime), 19, 11, 2019, 120)
SET IDENTITY_INSERT [dbo].[Calendar] OFF
SET IDENTITY_INSERT [dbo].[CalendarPerson] ON 

INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (1, 4, 1)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (2, 4, 2)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (6, 7, 1)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (7, 7, 2)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (8, 7, 3)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (21, 28, 1)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (22, 28, 3)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (23, 29, 1)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (30, 4, 1)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (31, 4, 2)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (32, 4, 1)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (33, 4, 2)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (43, 34, 2)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (44, 34, 9)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (45, 29, 2)
INSERT [dbo].[CalendarPerson] ([Id], [CalendarId], [PersonId]) VALUES (46, 34, 1)
SET IDENTITY_INSERT [dbo].[CalendarPerson] OFF
SET IDENTITY_INSERT [dbo].[ContactPerson] ON 

INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (1, 1, 0, N'4787-9863')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (2, 1, 1, N'JuanPerez@gmail.com')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (3, 1, 2, N'15-4334-5542')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (4, 2, 2, N'15-6762-7756')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (5, 3, 1, N'Carlos.Diaz@mail.com')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (6, 3, 0, N'7825-2323')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (16, 9, 0, N'4563-9972')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (17, 9, 2, N'15-5443-1211')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (18, 10, 0, N'4214-5832')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (19, 10, 2, N'15-8732-1144')
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (21, 2, 0, NULL)
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (22, 2, 1, NULL)
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (23, 3, 2, NULL)
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (24, 9, 1, NULL)
INSERT [dbo].[ContactPerson] ([Id], [PersonId], [ContactId], [Value]) VALUES (25, 10, 1, NULL)
SET IDENTITY_INSERT [dbo].[ContactPerson] OFF
SET IDENTITY_INSERT [dbo].[CalendarDetail] ON 

INSERT [dbo].[CalendarDetail] ([Id], [CalendarId], [Description], [Reminder], [Status]) VALUES (4, 4, N'Reunion del proyecto', 0, 2)
INSERT [dbo].[CalendarDetail] ([Id], [CalendarId], [Description], [Reminder], [Status]) VALUES (7, 7, N'Reporte de avance', 0, 1)
INSERT [dbo].[CalendarDetail] ([Id], [CalendarId], [Description], [Reminder], [Status]) VALUES (12, 28, N'Reunion con Gerencia', NULL, 1)
INSERT [dbo].[CalendarDetail] ([Id], [CalendarId], [Description], [Reminder], [Status]) VALUES (13, 29, N'Reunion de seguimiento', NULL, 0)
INSERT [dbo].[CalendarDetail] ([Id], [CalendarId], [Description], [Reminder], [Status]) VALUES (18, 34, N'Reunion de finalización de proyecto', NULL, 3)
SET IDENTITY_INSERT [dbo].[CalendarDetail] OFF
INSERT [dbo].[Holiday] ([Date], [Name]) VALUES (CAST(N'2019-11-18T00:00:00.000' AS DateTime), N'Día de la Soberanía')
INSERT [dbo].[Holiday] ([Date], [Name]) VALUES (CAST(N'2019-12-08T00:00:00.000' AS DateTime), N'Día de la virgen')
INSERT [dbo].[Holiday] ([Date], [Name]) VALUES (CAST(N'2019-12-25T00:00:00.000' AS DateTime), N'Navidad')
INSERT [dbo].[Holiday] ([Date], [Name]) VALUES (CAST(N'2020-01-01T00:00:00.000' AS DateTime), N'Año nuevo')

﻿CREATE TABLE [dbo].[ContactPerson] (
    [Id]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [PersonId]  BIGINT         NOT NULL,
    [ContactId] INT            NOT NULL,
    [Value]     VARCHAR (1000) NULL,
    CONSTRAINT [PK_ContactPerson] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ContactPerson_ContactPerson] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[Contact] ([Id]),
    CONSTRAINT [FK_ContactPerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([Id])
);


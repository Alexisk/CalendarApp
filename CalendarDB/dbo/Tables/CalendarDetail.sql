﻿CREATE TABLE [dbo].[CalendarDetail] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [CalendarId]  BIGINT         NOT NULL,
    [Description] VARCHAR (1000) NULL,
    [Reminder]    INT            NULL,
    [Status]      INT            NULL,
    CONSTRAINT [PK_CalendarDetail_1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CalendarDetail_Calendar] FOREIGN KEY ([CalendarId]) REFERENCES [dbo].[Calendar] ([Id])
);




﻿CREATE TABLE [dbo].[Calendar] (
    [Id]    BIGINT   IDENTITY (1, 1) NOT NULL,
    [Date]  DATETIME NULL,
		[Day] INT      NULL,
    [Month] INT      NULL,
    [Year] INT NULL, 
    [ElapsedTime] INT NULL, 
    CONSTRAINT [PK_Calendar] PRIMARY KEY CLUSTERED ([Id] ASC)
);


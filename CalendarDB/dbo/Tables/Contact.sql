﻿CREATE TABLE [dbo].[Contact] (
    [Id]   INT           NOT NULL,
    [Name] VARCHAR (100) NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([Id] ASC)
);


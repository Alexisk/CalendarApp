﻿CREATE TABLE [dbo].[CalendarPerson] (
    [Id]         BIGINT IDENTITY (1, 1) NOT NULL,
    [CalendarId] BIGINT NULL,
    [PersonId]   BIGINT NULL,
    CONSTRAINT [PK_CalendarPerson] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CalendarPerson_Calendar] FOREIGN KEY ([CalendarId]) REFERENCES [dbo].[Calendar] ([Id]),
    CONSTRAINT [FK_CalendarPerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([Id])
);


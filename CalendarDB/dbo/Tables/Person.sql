﻿CREATE TABLE [dbo].[Person] (
    [Id]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (1000) NULL,
    [LastName] VARCHAR (1000) NULL,
    CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([Id] ASC)
);


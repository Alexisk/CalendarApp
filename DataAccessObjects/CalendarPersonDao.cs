﻿using DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using DataModel;
using Microsoft.Extensions.Configuration;

namespace Dao
{
  public class CalendarPersonDao : Dao<CalendarPerson>
  {
	  public CalendarPersonDao(IConfiguration config) : base(config)
	  {

	  }

		public IList<CalendarPerson> GetAllPersonsByCalendarId(long calendarId)
	  {
		  try
		  {
			  //Create a custom visitor to send an expression<Func<T,bool>> and create a string based on it.
			  return this.GetAllElements($"CalendarId = {calendarId}");
			}
		  catch (Exception e)
		  {
				throw new DaoException(e.Message);
		  }
	  }

	  public CalendarPerson GetElementByPersonId(long personId)
	  {
		  try
		  {
			  return this.GetAllElements($"PersonId = {personId}").FirstOrDefault();
			}
		  catch (Exception e)
		  {
				throw new DaoException(e.Message);
			}
	  }
  }
}

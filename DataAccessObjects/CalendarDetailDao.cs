﻿using DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using DataModel;
using Microsoft.Extensions.Configuration;

namespace Dao
{
  public class CalendarDetailDao : Dao<CalendarDetail>
  {
	  public CalendarDetailDao(IConfiguration config) : base(config)
	  {

	  }

		public IList<CalendarDetail> GetAllElements(int? year, int? month, int? day)
    {
	    try
	    {
				var list = new List<CalendarDetail>();

				string query = @"SELECT Calendar.Id CId, Calendar.Date CDate, Calendar.Year CYear, Calendar.Month CMonth, Calendar.Day CDay, Calendar.ElapsedTime CElapsedTime, CalendarDetail.* FROM Calendar 
	                      INNER JOIN CalendarDetail on CalendarDetail.CalendarId = Calendar.Id";

				bool whereFlag = false;
				if (year.HasValue)
				{
					query += $" WHERE Calendar.Year = @Year";
					whereFlag = true;
				}

				if (month.HasValue)
				{
					query += !whereFlag ? $" WHERE " : $" AND ";
					query += $"Calendar.Month = @Month";
					whereFlag = true;
				}

				if (day.HasValue)
				{
					query += !whereFlag ? $" WHERE " : $" AND ";
					query += $"Calendar.Day = @Day";
					whereFlag = true;
				}

				using (var dataSource = new SqlConnection(connectionString))
				{
					dataSource.Open();
					using (var command = new SqlCommand(query, dataSource))
					{
						if (day.HasValue) command.Parameters.AddWithValue("@Day", day);
						if (month.HasValue) command.Parameters.AddWithValue("@Month", month);
						if (year.HasValue) command.Parameters.AddWithValue("@Year", year);

						var reader = command.ExecuteReader();

						while (reader.Read())
						{
							var detail = new CalendarDetail();

							var i = new Calendar();
							i.Id = Convert.ToInt64(reader["CId"]);
							i.Date = Convert.ToDateTime(reader["CDate"]);
							i.Year = Convert.ToInt32(reader["CYear"]);
							i.Month = Convert.ToInt32(reader["CMonth"]);
							i.Day = Convert.ToInt32(reader["CDay"]);
							i.ElapsedTime = Convert.ToInt32(reader["CElapsedTime"]);

							detail.Calendar = i;
							detail.Description = reader["Description"].ToString();
							detail.Reminder = reader["Reminder"] != DBNull.Value ? Convert.ToInt32(reader["Reminder"]) : 0;
							detail.Status = (DataModel.Enum.CalendarDetailStatus)Convert.ToInt32(reader["Status"]);

							list.Add(detail);
						}

					}
				}
				return list;
			}
	    catch (Exception e)
	    {
				throw new DaoException(e.Message);
			}
      
    }

		[Obsolete]
    public IList<CalendarDetail> GetAllElements(int year, int month)
    {
      var list = new List<CalendarDetail>();

      string query = @"SELECT Calendar.Id CId, Calendar.Date CDate, Calendar.Month CMonth, Calendar.ElapsedTime CElapsedTime,CalendarDetail.* FROM Calendar 
	                      INNER JOIN CalendarDetail on CalendarDetail.CalendarId = Calendar.Id
                        WHERE Calendar.Month = @Month AND Calendar.Year = @Year";

      using (var dataSource = new SqlConnection(connectionString))
      {
        dataSource.Open();
        using (var command = new SqlCommand(query, dataSource))
        {
          command.Parameters.AddWithValue("@Month", month);
          command.Parameters.AddWithValue("@Year", year);
          var reader = command.ExecuteReader();
          
          while (reader.Read())
          {
            var detail = new CalendarDetail();

            var i = new Calendar();
            i.Id = Convert.ToInt64(reader["CId"]);
            i.Date = Convert.ToDateTime(reader["CDate"]);
            i.Month = Convert.ToInt32(reader["CMonth"]);
            i.ElapsedTime = Convert.ToInt32(reader["CElapsedTime"]);

            detail.Calendar = i;
            detail.Description = reader["Description"].ToString();
            detail.Reminder = reader["Reminder"] != DBNull.Value ? Convert.ToInt32(reader["Reminder"]) : 0;
            detail.Status = (DataModel.Enum.CalendarDetailStatus)Convert.ToInt32(reader["Status"]);

            list.Add(detail);
          }

        }
      }
      return list;

    }

    public IList<Person> GetEntriesForId(long calendarId)
    {
	    try
	    {
		    var list = new List<Person>();

		    string query = @"SELECT Person.Id pId, Person.Name pName, Person.LastName pLastName FROM CalendarPerson 
	                      INNER JOIN Person on CalendarPerson.PersonId = Person.Id
                        WHERE CalendarPerson.CalendarId = @Id";

		    using (var dataSource = new SqlConnection(connectionString))
		    {
			    dataSource.Open();
			    using (var command = new SqlCommand(query, dataSource))
			    {
				    command.Parameters.AddWithValue("@Id", calendarId);

				    var reader = command.ExecuteReader();

				    while (reader.Read())
				    {
					    var person = new Person();

					    person.Id = Convert.ToInt32(reader["pId"].ToString());
					    person.Name = reader["pName"].ToString();
					    person.LastName = reader["pLastName"].ToString();

					    list.Add(person);
				    }
			    }
		    }
		    return list;
			}
	    catch (Exception e)
	    {
				throw new DaoException(e.Message);
			}
      

    }
  }
}

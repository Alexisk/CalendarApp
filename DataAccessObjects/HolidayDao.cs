﻿using System;
using System.Data.SqlClient;
using DataModel;
using DataModel.Entities;
using Microsoft.Extensions.Configuration;

namespace Dao
{
  public class HolidayDao : Dao<Holiday>
  {
	  public HolidayDao(IConfiguration config) : base(config)
	  {

	  }

		public void DeleteElement(DateTime id)
	  {
		  try
		  {
			  var cmd = $@"DELETE FROM [dbo].[Holiday] WHERE Date = CONVERT( DATETIME, '{id:d}', 103 ) ";

			  using (var connection = new SqlConnection(connectionString))
			  {
				  connection.Open();

				  using (var command = new SqlCommand(cmd, connection))
				  {
					  command.ExecuteNonQuery();
				  }
			  }

		  }
		  catch (Exception e)
		  {
			  throw new DaoException(e.Message);
		  }
	  }
	}
}
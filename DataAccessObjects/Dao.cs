﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using DataModel;
using DataModel.Entities;
using Microsoft.Extensions.Configuration;

namespace Dao
{
  public class Dao<T> where T : new()
  {
	  protected string connectionString;

    public Dao(IConfiguration config)
    {
	    connectionString = config.GetSection("connectionString").Value;
		}

    public virtual IList<T> GetAllElements(string filter = null)
    {
			try
			{
				var result  = new List<T>();

				var columnsNames = new List<string>();
				var joinStatements = new List<string>();
				
				foreach (var property in typeof(T).GetProperties())
				{
					if (property.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
						continue;

					//TODO It could be a recursive function
					if (property.PropertyType.Namespace == typeof(DataModelHelper).Namespace)
					{
						foreach (var subProp in property.PropertyType.GetProperties())
						{
							//TODO The Generic DAO is not ready to process Collections
							if (subProp.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(subProp.PropertyType))
								continue;

							columnsNames.Add($"{property.Name}.{subProp.Name} {property.Name}_{subProp.Name}");

						}
						joinStatements.Add($"INNER JOIN {property.Name} ON {property.Name}{DataModelHelper.Identifier} = {property.Name}.{DataModelHelper.Identifier}");
					}
					else
					{
						columnsNames.Add($"{typeof(T).Name}.{property.Name} {typeof(T).Name}_{property.Name}");
					}
				}

				var cmd = $@"SELECT 
									{string.Join(',', columnsNames)}
									FROM {typeof(T).Name} ";

				cmd += joinStatements.Count > 0 ? string.Join(' ', joinStatements) : string.Empty;

				if (!string.IsNullOrEmpty(filter))
					cmd += $" WHERE {filter}";

				using (var connection = new SqlConnection(connectionString))
				{
					connection.Open();
					using (var command = new SqlCommand(cmd, connection))
					{
						var reader = command.ExecuteReader();

						var columns = new List<string>();

						for (int i = 0; i < reader.FieldCount; i++)
						{
							columns.Add(reader.GetName(i));
						}

						while (reader.Read())
						{
							var item = new T();
							var innerObjects = new Dictionary<string, object>();
							
							foreach (var property in typeof(T).GetProperties().Where(x => x.PropertyType.Namespace == typeof(DataModelHelper).Namespace))
							{
								innerObjects.Add(property.Name, Activator.CreateInstance(property.PropertyType));
								property.SetValue(item, innerObjects[property.Name]);
							}
							
							foreach (var rawCol in columns)
							{
								var cellValue = reader[rawCol];
								if (cellValue == DBNull.Value)
									continue;

								var splitCol = rawCol.Split('_');
								var entityCol = splitCol[0];
								var col = splitCol[1];

								if (entityCol == typeof(T).Name)
								{
									PropertyInfo prop = item.GetType().GetProperty(col, BindingFlags.Public | BindingFlags.Instance);
									if (null != prop && prop.CanWrite)
									{
										SetObjectValue(prop, item, reader[rawCol]);
									}
								}
								else
								{

									PropertyInfo prop = item.GetType().GetProperty(entityCol, BindingFlags.Public | BindingFlags.Instance);
									if (null != prop && prop.CanWrite)
									{
										var subProp = prop.PropertyType.GetProperty(col, BindingFlags.Public | BindingFlags.Instance);
										SetObjectValue(subProp, innerObjects[prop.Name], reader[rawCol]);
									}
								}
							}

							result.Add(item);
						}

						return result;
					}
				}
			}
			catch (Exception e)
			{
				throw new DaoException(e.Message);
			}

		}

		public virtual T GetElementById(long id)
    {
	    try
	    {
				var item = new T();
				
		    var columnsNames = new List<string>();
		    var joinStatements = new List<string>();
		    var innerObjects = new Dictionary<string, object>();
				foreach (var property in typeof(T).GetProperties())
		    {
			    if (property.PropertyType.Namespace == typeof(DataModelHelper).Namespace)
			    {
				    if (property.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
					    continue;
						
						innerObjects.Add(property.Name, Activator.CreateInstance(property.PropertyType));
						property.SetValue(item, innerObjects[property.Name]);
				    foreach (var subProp in property.PropertyType.GetProperties())
				    {
					    if (subProp.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(subProp.PropertyType))
						    continue;

							columnsNames.Add($"{property.Name}.{subProp.Name} {property.Name}_{subProp.Name}");

						}
				    joinStatements.Add($"INNER JOIN {property.Name} ON {property.Name}{DataModelHelper.Identifier} = {property.Name}.{DataModelHelper.Identifier}");
			    }
			    else
			    {
				    columnsNames.Add($"{typeof(T).Name}.{property.Name} {typeof(T).Name}_{property.Name}");
					}
		    }

		    var cmd = $@"SELECT 
									{string.Join(',',columnsNames)}
									FROM {typeof(T).Name} ";

		    cmd += joinStatements.Count > 0 ? string.Join(' ', joinStatements) : string.Empty;
				cmd += $" WHERE {typeof(T).Name}.{DataModelHelper.Identifier} = {id}";


				using (var connection = new SqlConnection(connectionString))
				{
					connection.Open();
					using (var command = new SqlCommand(cmd, connection))
					{
						var reader = command.ExecuteReader();

						var columns = new List<string>();

						for (int i = 0; i < reader.FieldCount; i++)
						{
							columns.Add(reader.GetName(i));
						}

						while (reader.Read())
						{
							foreach (var rawCol in columns)
							{
								var cellValue = reader[rawCol];
								if(cellValue == DBNull.Value)
									continue;
								
								var splitCol = rawCol.Split('_');
								var entityCol = splitCol[0];
								var col = splitCol[1];

								if (entityCol == typeof(T).Name)
								{
									PropertyInfo prop = item.GetType().GetProperty(col, BindingFlags.Public | BindingFlags.Instance);
									if (null != prop && prop.CanWrite)
									{
										SetObjectValue(prop, item, reader[rawCol]);
									}
								}
								else
								{

									PropertyInfo prop = item.GetType().GetProperty(entityCol, BindingFlags.Public | BindingFlags.Instance);
									if (null != prop && prop.CanWrite)
									{
										var subProp = prop.PropertyType.GetProperty(col, BindingFlags.Public | BindingFlags.Instance);
										SetObjectValue(subProp, innerObjects[prop.Name], reader[rawCol]);
									}
								}
							}

							break;
						}

						return item;
					}
				}
	    }
	    catch (Exception e)
	    {
		    throw new DaoException(e.Message);
	    }
			
    }

    public virtual void InsertElement(T entity)
    {
	    try
	    {
		    var id = entity.GetType().GetProperties().FirstOrDefault(x => x.Name == DataModelHelper.Identifier);

				var properties = entity.GetType().GetProperties();

				if (id != null)
					properties = properties.Where(x => x.Name != id.Name).ToArray();
				
				var data = new Dictionary<string, object>();
		    foreach (var property in properties)
		    {
			    if (property.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
				    continue;

					if (property.PropertyType.Namespace == typeof(DataModelHelper).Namespace)
					{
						var obj = property.GetValue(entity);
						var objIdValue = obj.GetType().GetProperty(DataModelHelper.Identifier).GetValue(obj);
						data.Add(property.Name + DataModelHelper.Identifier, objIdValue);
					}
					else
					{
						data.Add(property.Name, property.GetValue(entity));
					}
		    }
				
				var values = data.Values.Select(ConvertMetaDataToString);

				var cmd = $"INSERT INTO {entity.GetType().Name} ({string.Join(',', data.Keys)}) VALUES ({string.Join(',', values)});";
				
				if (id != null)
					cmd += " SELECT CAST(SCOPE_IDENTITY() AS BIGINT);";

				using (var connection = new SqlConnection(connectionString))
				{
					connection.Open();

					using (var command = new SqlCommand(cmd, connection))
					{
						if (id != null)
						{
							var result = command.ExecuteScalar();
							id.SetValue(entity, result);
						}
						else
						{
							command.ExecuteNonQuery();
						}
					}
				}
				
	    }
	    catch (Exception e)
	    {
		    throw new DaoException(e.Message);
	    }

		}

    public virtual void UpdateElement(T entity)
    {
	    try
	    {
				var cmd = $@"UPDATE [dbo].[{typeof(T).Name}] SET ";
				var propertyStatements = new List<string>();
				long idValue = 0;
		    foreach (var property in typeof(T).GetProperties())
		    {
			    if (property.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
				    continue;

					var value = property.GetValue(entity);

					if (property.Name == DataModelHelper.Identifier)
					{
						idValue = Convert.ToInt64(value.ToString());
						continue;
					}

			    
					if (property.PropertyType.Namespace == typeof(DataModelHelper).Namespace)
			    {
				    var propId = property.PropertyType.GetProperties()
					    .FirstOrDefault(x => x.Name == DataModelHelper.Identifier);

				    var propIdValue = propId.GetValue(value);
				    propertyStatements.Add($"[{property.Name}{DataModelHelper.Identifier}] = {ConvertMetaDataToString(propIdValue)} ");
					}
			    else
			    {
				    propertyStatements.Add($"[{property.Name}] = {ConvertMetaDataToString(value)} ");
			    }
		    }

		    cmd += string.Join(',', propertyStatements);
		    cmd += $"WHERE id = {idValue}";
				
		    using (var connection = new SqlConnection(connectionString))
		    {
			    connection.Open();

			    using (var command = new SqlCommand(cmd, connection))
			    {
				    command.ExecuteNonQuery();
			    }
		    }

			}
			catch (Exception e)
	    {
				throw new DaoException(e.Message);
			}
    }
		
    public virtual void DeleteElement(long id)
    {
	    try
	    {
		    var cmd = $@"DELETE FROM [dbo].[{typeof(T).Name}] WHERE id = {id}";
				
		    using (var connection = new SqlConnection(connectionString))
		    {
			    connection.Open();

			    using (var command = new SqlCommand(cmd, connection))
			    {
				    command.ExecuteNonQuery();
			    }
		    }

			}
			catch (Exception e)
	    {
		    throw new DaoException(e.Message);
	    }
		}
		
		#region Helpers

		private static void SetObjectValue(PropertyInfo prop, object item, object value)
		{

			//Check Enum
			if (prop.PropertyType is Enum)
			{
				prop.SetValue(item, Enum.ToObject(prop.PropertyType, (int)value), null);
				return;
			}

			var nullableType = Nullable.GetUnderlyingType(prop.PropertyType);
			if (nullableType != null && nullableType.IsEnum)
			{
				prop.SetValue(item, Enum.ToObject(nullableType, (int)value), null);
				return;
			}

			prop.SetValue(item, value, null);
		}
		
		private string ConvertMetaDataToString(object t)
		{
			if (t == null)
				return "null";

			if (t is DateTime)
				return $"'{(DateTime)t:s}'";

			if (t is string)
				return $"'{t}'";

			if (t.GetType().BaseType.Name == nameof(Enum))
			{
				object underlyingValue = Convert.ChangeType(t, Enum.GetUnderlyingType(t.GetType()));
				return underlyingValue.ToString();
			}

			return t.ToString();
		}

		#endregion
	}
}

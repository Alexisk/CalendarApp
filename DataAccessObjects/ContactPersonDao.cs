﻿using System;
using System.Collections.Generic;
using DataModel;
using DataModel.Entities;
using Microsoft.Extensions.Configuration;

namespace Dao
{
  public class ContactPersonDao : Dao<ContactPerson>
  {
	  public ContactPersonDao(IConfiguration config) : base(config)
	  {

	  }

		public IList<ContactPerson> GetAllContactsByPersonId(long personId)
	  {
		  try
		  {
			  return this.GetAllElements($"PERSONID = {personId}");
			}
		  catch (Exception e)
		  {
				throw new DaoException(e.Message);
			}
	  }
  }
}
﻿using DataModel.Entities;
using DataModel.Enum;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DataModel;
using Microsoft.Extensions.Configuration;

namespace Dao
{
  public class PersonDao : Dao<Person>
  {
	  public PersonDao(IConfiguration config) : base(config)
	  {

	  }

		public override IList<Person> GetAllElements(string filter = null)
    {
	    try
	    {
				var list = new List<Person>();

				var contactList = new List<Contact>();

				string query = @"SELECT Person.Id PId, Person.Name PName, Person.LastName PLastName, Contact.Name CName, ContactPerson.ContactId CPContactId, ContactPerson.Value CPValue
                        FROM [Person]
                        INNER JOIN ContactPerson ON ContactPerson.PersonId = Person.Id
                        INNER JOIN Contact ON ContactPerson.ContactId = Contact.Id";

				if (!string.IsNullOrEmpty(filter))
					query += $" WHERE {filter}";

				using (var dataSource = new SqlConnection(connectionString))
				{
					dataSource.Open();
					using (var command = new SqlCommand(query, dataSource))
					{
						var reader = command.ExecuteReader();

						while (reader.Read())
						{
							var id = Convert.ToInt64(reader["PId"]);
							var person = list.FirstOrDefault(p => p.Id == id);
							if (person == null)
							{
								person = new Person();
								person.Id = id;
								person.Name = reader["PName"].ToString();
								person.LastName = reader["PLastName"].ToString();
								person.Contacts = new List<ContactPerson>();
								list.Add(person);
							}

							var cid = ((ContactType)Convert.ToInt64(reader["CPContactId"]));
							var contact = contactList.FirstOrDefault(c => c.Id == cid);
							if (contact == null)
							{
								contact = new Contact();
								contact.Id = cid;
								contact.Name = reader["CName"].ToString();

								contactList.Add(contact);
							}

							var contactPerson = new ContactPerson();
							contactPerson.Contact = contact;
							contactPerson.Id = Convert.ToInt64(reader["CPContactId"]);
							contactPerson.Value = reader["CPValue"].ToString();
							contactPerson.Person = person;

							person.Contacts.Add(contactPerson);

						}

					}
				}
				return list;
	    }
			catch (Exception e)
	    {
				throw new DaoException(e.Message);
			}
    }
		
	  public Person FindPersonBy(string name, string lastName)
	  {
		  try
		  {
			  return base.GetAllElements($"NAME = '{name}' AND LASTNAME = '{lastName}'").FirstOrDefault();
			}
		  catch (Exception e)
		  {
				throw new DaoException(e.Message);
			}
	  }

	  public override Person GetElementById(long id)
	  {
		  try
		  {
			  return this.GetAllElements($"PERSONID = {id}").FirstOrDefault();
			}
		  catch (Exception e)
		  {
				throw new DaoException(e.Message);
			}
	  }
  }
}
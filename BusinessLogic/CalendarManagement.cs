﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using Dao;
using DataModel;
using DataModel.DTO;
using DataModel.Entities;
using DataModel.Enum;
using DataModel.Exceptions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BusinessLogic
{
	public class CalendarManagement : ICalendarManagement
	{
		#region Field

		private readonly ILogger<CalendarManagement> _logger;

		#endregion

		#region Proeprties

		public CalendarDao CalendarDao { get; set; }

		public CalendarDetailDao CalendarDetailDao { get; set; }

		public CalendarPersonDao CalendarPersonDao { get; set; }

		public HolidayDao HolidayDao { get; set; }

		#endregion

		#region Constructor

		public CalendarManagement(IConfiguration config, ILogger<CalendarManagement> logger)
		{
			_logger = logger;
			CalendarDetailDao = new CalendarDetailDao(config);
			CalendarPersonDao = new CalendarPersonDao(config);
			CalendarDao = new CalendarDao(config);
			HolidayDao = new HolidayDao(config);
		}

		#endregion

		#region Interface Implementation

		public IList<CalendarInfo> GetMonthInfo(int year, int month)
		{
			var result = new List<CalendarInfo>();

			try
			{
				var details = CalendarDetailDao.GetAllElements(year, month, null);
				var holidays = HolidayDao.GetAllElements();

				for (int i = 1; i <= DateTime.DaysInMonth(year, month); i++)
				{
					var dt = new DateTime(year, month, i);
					var item = new CalendarInfo();
					var dayDetails = details.Where(x => x.Calendar.Date.Day == i).ToList();
					var calendar = dayDetails.FirstOrDefault()?.Calendar;

					var holiday = holidays.FirstOrDefault(x => x.Date == dt);
					if (holiday != null)
					{
						item.IsHoliday = true;
						item.HolidayDescription = holiday.Name;
					}

					item.DayWeekNumber = dt.DayOfWeek;
					item.DayNumber = dt.Day;
					item.Calendar = calendar;
					item.Details = dayDetails;
					result.Add(item);
				}

				return result;

			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al obtener la información de la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al obtener la información del mes.");
				throw new BusinessException(e.Message);
			}

		}

		public IList<CalendarDetail> GetDetails()
		{
			try
			{
				return CalendarDetailDao.GetAllElements();
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al obtener la información de la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al obtener los detalles del calendario.");
				throw new BusinessException(e.Message);
			}
		}

		public IList<CalendarDetail> GetDetails(int? year, int? month, int? day)
		{
			try
			{
				return CalendarDetailDao.GetAllElements(year, month, day);
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al obtener la información de la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al obtener los detalles del calendario.");
				throw new BusinessException(e.Message);
			}

		}

		public IList<Person> GetEntriesForId(long calendarId)
		{
			try
			{
				return CalendarDetailDao.GetEntriesForId(calendarId);
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al obtener la información de la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al obtener los detalles de las personas.");
				throw new BusinessException(e.Message);
			}
		}

		public void SaveAppointment(string description, DateTime date, int elapsedTime, IList<long> entries)
		{
			try
			{
				using (TransactionScope scope = new TransactionScope())
				{
					var calendar = new Calendar
					{
						Year = date.Year,
						Month = date.Month,
						Day = date.Day,
						Date = date,
						ElapsedTime = elapsedTime
					};

					//set id on calendar
					CalendarDao.InsertElement(calendar);

					CalendarDetailDao.InsertElement(new CalendarDetail
					{
						Calendar = calendar,
						Description = description,
						Status = CalendarDetailStatus.Pending
					});

					foreach (var entry in entries)
					{
						CalendarPersonDao.InsertElement(new CalendarPerson
						{
							Calendar = calendar,
							Person = new Person() { Id = entry }
						});
					}

					scope.Complete();
				}
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al procesar la información en la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al obtener intentar guardar la reunion.");
				throw new BusinessException(e.Message);
			}
		}

		public CalendarDetail GetCalendarWithDetailById(long id)
		{
			try
			{
				return CalendarDetailDao.GetElementById(id);
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al obetener la información en la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al obtener el calendario con el detalle.");
				throw new BusinessException(e.Message);
			}
		}

		public void UpdateAppointment(long detailId, CalendarDetailStatus status, string description, DateTime date, int elapsedTime,
			List<long> entries)
		{
			try
			{
				using (TransactionScope scope = new TransactionScope())
				{
					//Update Detail
					var detail = CalendarDetailDao.GetElementById(detailId);
					detail.Description = description;
					detail.Status = status;
					CalendarDetailDao.UpdateElement(detail);

					//Update Calendar
					var calendar = detail.Calendar;
					calendar.Date = date;
					calendar.Year = date.Year;
					calendar.Month = date.Month;
					calendar.Day = date.Day;
					calendar.ElapsedTime = elapsedTime;
					CalendarDao.UpdateElement(calendar);

					//UpdateEntries
					var currentEntries = CalendarPersonDao.GetAllPersonsByCalendarId(calendar.Id);

					//Delete old entries
					var deleteEntries = currentEntries.Where(x => !entries.Contains(x.Person.Id));
					foreach (var entry in deleteEntries)
					{
						CalendarPersonDao.DeleteElement(entry.Id);
					}

					var newEntries = entries.Where(x => currentEntries.All(i => i.Person.Id != x));
					foreach (var entry in newEntries)
					{
						CalendarPersonDao.InsertElement(new CalendarPerson
						{
							Calendar = calendar,
							Person = new Person() { Id = entry }
						});
					}

					scope.Complete();
				}
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al actualizar la información en la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al actualizar la reunion.");
				throw new BusinessException(e.Message);
			}
		}

		public void DeleteAppointment(long detailId)
		{
			try
			{
				using (TransactionScope scope = new TransactionScope())
				{

					var detail = CalendarDetailDao.GetElementById(detailId);

					var entries = CalendarPersonDao.GetAllPersonsByCalendarId(detail.Calendar.Id);

					foreach (var entry in entries)
					{
						CalendarPersonDao.DeleteElement(entry.Id);
					}

					CalendarDetailDao.DeleteElement(detailId);

					CalendarDao.DeleteElement(detail.Calendar.Id);

					scope.Complete();
				}
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al eliminar la información en la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al elimniar la reunion.");
				throw new BusinessException(e.Message);
			}
		}

		public void ImportHolidays(Holiday[] newHolidays)
		{
			try
			{
				using (TransactionScope scope = new TransactionScope())
				{
					var currentHolidays = HolidayDao.GetAllElements();

					foreach (var currentHoliday in currentHolidays)
					{
						HolidayDao.DeleteElement(currentHoliday.Date);
					}

					foreach (var holiday in newHolidays)
					{
						HolidayDao.InsertElement(holiday);
					}

					scope.Complete();
				}

			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al guardar la información en la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al guardar los feriados.");
				throw new BusinessException(e.Message);
			}
		}

		#endregion

	}
}

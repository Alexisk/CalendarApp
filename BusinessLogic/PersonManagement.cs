﻿using Dao;
using DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using DataModel;
using DataModel.Exceptions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BusinessLogic
{
  public class PersonManagement : IPersonManagement
  {
	  #region Field

	  private readonly ILogger<PersonManagement> _logger;

		#endregion

		#region Properties

		public PersonDao PersonDao { get; set; }

		public ContactDao ContactDao { get; set; }

		public ContactPersonDao ContactPersonDao { get; set; }

		public CalendarPersonDao CalendarPersonDao { get; set; }

		#endregion

		#region Constructor

		public PersonManagement(IConfiguration config, ILogger<PersonManagement> logger)
		{
			_logger = logger;
			PersonDao = new PersonDao(config);
			ContactDao = new ContactDao(config);
			ContactPersonDao = new ContactPersonDao(config);
			CalendarPersonDao = new CalendarPersonDao(config);
		}

		#endregion

		#region Interface Implementation

		public IList<Person> GetAllPersons()
		{
			try
			{
				var result = PersonDao.GetAllElements();

				return result;
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al obtener la información de la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al obtener todas las personas.");
				throw new BusinessException(e.Message);
			}
		}

		public void UpdateRegister(long personId, string name, string lastName, string phone, string cellphone, string mail)
		{
			try
			{
				if (string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(cellphone) && string.IsNullOrEmpty(mail))
					throw new Exception("Debe registrar al menos un contacto");

				using (TransactionScope scope = new TransactionScope())
				{
					//Update Detail
					var detail = PersonDao.GetElementById(personId);
					detail.Name = name;
					detail.LastName = lastName;

					PersonDao.UpdateElement(detail);

					//UpdateEntries
					var items = ContactPersonDao.GetAllContactsByPersonId(personId);

					UpdateContact("Telefono", phone, items);
					UpdateContact("Celular", cellphone, items);
					UpdateContact("Email", mail, items);

					scope.Complete();
				}
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al actualizar la información de la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al intentar actualizar la persona.");
				throw new BusinessException(e.Message);
			}
		}

		public void SaveRegister(string name, string lastName, string phone, string cellphone, string mail)
		{
			try
			{
				if (string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(cellphone) && string.IsNullOrEmpty(mail))
					throw new Exception("Debe registrar al menos un contacto");

				using (TransactionScope scope = new TransactionScope())
				{
					var contacts = ContactDao.GetAllElements();

					var person = PersonDao.FindPersonBy(name, lastName);
					if (person != null)
						throw new Exception("Ya existe un usuario con ese nombre y apellido");

					person = new Person()
					{
						Name = name,
						LastName = lastName
					};

					PersonDao.InsertElement(person);

					CreateContact("Telefono", phone, contacts, person);

					CreateContact("Celular", cellphone, contacts, person);

					CreateContact("Email", mail, contacts, person);

					scope.Complete();
				}
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al guardar la información de la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al guardar la persona");
				throw new BusinessException(e.Message);
			}
		}

		public void DeleteRegister(long personId)
		{
			try
			{

				var calendar = CalendarPersonDao.GetElementByPersonId(personId);
				if (calendar != null)
					throw new Exception("No se puede eliminar una persona que tiene una reunion asignada.");

				using (TransactionScope scope = new TransactionScope())
				{

					var contactPersons = ContactPersonDao.GetAllContactsByPersonId(personId);

					foreach (var contactPerson in contactPersons)
					{
						ContactPersonDao.DeleteElement(contactPerson.Id);
					}

					PersonDao.DeleteElement(personId);

					scope.Complete();
				}
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al eliminar la información de la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al eliminar la persona");
				throw new BusinessException(e.Message);
			}
		}

		public Person GetPersonById(long id)
		{
			try
			{
				return PersonDao.GetElementById(id);
			}
			catch (DaoException e)
			{
				_logger.LogError(e, "Error al obtener la información de la base.");
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, "Error al obtener la persona");
				throw new BusinessException(e.Message);
			}
		}

		#endregion

		#region Helpers

		private void UpdateContact(string key, string value, IList<ContactPerson> contactPersons)
		{
			var contactPerson = contactPersons.FirstOrDefault(x => x.Contact.Name == key);

			if (contactPerson != null)
			{
				contactPerson.Value = value;
				ContactPersonDao.UpdateElement(contactPerson);
			}
			else
			{
				var contact = ContactDao.GetAllElements().FirstOrDefault(x => x.Name == key);
				if (contact == null)
					throw new Exception($"No existe el tipo de contacto {key} en la base");

				contactPerson = new ContactPerson()
				{
					Contact = contact,
					Person = contactPersons.FirstOrDefault().Person,
					Value = value
				};

				ContactPersonDao.InsertElement(contactPerson);
			}
		}

		private void CreateContact(string key, string value, IList<Contact> contacts, Person person)
		{
			if (!string.IsNullOrEmpty(value))
			{
				var contact = contacts.FirstOrDefault(x => x.Name == key);

				if (contact == null)
					throw new Exception($"No existe el tipo de contacto {key} en la base");

				ContactPersonDao.InsertElement(new ContactPerson()
				{
					Contact = contact,
					Person = person,
					Value = value
				});
			}
		}
		
		#endregion
	}
}

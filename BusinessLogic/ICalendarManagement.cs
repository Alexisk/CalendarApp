﻿using System;
using System.Collections.Generic;
using DataModel.DTO;
using DataModel.Entities;
using DataModel.Enum;

namespace BusinessLogic
{
  public interface ICalendarManagement
  {
    IList<CalendarInfo> GetMonthInfo(int year, int month);

    IList<CalendarDetail> GetDetails(int? year, int? month, int? day);

    IList<CalendarDetail> GetDetails();

		IList<Person> GetEntriesForId(long calendarId);

    void SaveAppointment(string description, DateTime date, int elapsedTime, IList<long> entries);

    CalendarDetail GetCalendarWithDetailById(long id);

    void UpdateAppointment(long detailId, CalendarDetailStatus status, string description, DateTime date, int elapsedTime, List<long> entries);

    void DeleteAppointment(long detailId);

    void ImportHolidays(Holiday[] data);
  }
}
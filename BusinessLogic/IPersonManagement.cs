﻿using DataModel.Entities;
using System.Collections.Generic;

namespace BusinessLogic
{
  public interface IPersonManagement
  {
    IList<Person> GetAllPersons();

    void UpdateRegister(long personId, string name, string lastName, string phone, string cellphone, string modelMail);

    void SaveRegister(string name, string lastName, string phone, string cellphone, string mail);

    void DeleteRegister(long personId);

    Person GetPersonById(long id);
  }
}